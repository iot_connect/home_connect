// Load Models
const dataObject = require('../models/dataObjectModel');

// ---ADD--- //
// Permet à un objet d'ajouter une valeur remonté
const ADD_DATA_OBJECT = async (topic, value, unite) => { // id de l'obj qui remonte l'information | value est la valeur remonté par l'objet | unite est l'unité de la valeur remonté
    const addData = await dataObject.create({"obj" : topic, "value" : value, "unite" : unite, "datetime" : "$$NOW"});
    return addData;
};
module.exports = ADD_DATA_OBJECT();

// ---GET--- //
// Retourne toutes les valeurs des objets connectés
const FIND_DATA_OBJECT = async () => {
    const findData = await dataObject.find({});
    return findData;
};
module.exports = FIND_DATA_OBJECT();

// Retourne un objet connecté en fournissant l'ID
const FIND_THIS_DATA_OBJECT = async topic => { // Id de l'objet
    const findThisData = await dataObject.findOne({"obj" : topic});
    return findThisData;
};
module.exports = FIND_THIS_DATA_OBJECT();

// Récupère les 5 derniers élèments
const FIND_FIVE_LAST_DATA_OBJECT = async () => {
    const findThisDatas = dataObject.find().sort({"datetime" : -1}).limit(5);
    return findThisDatas;
};
module.exports = FIND_FIVE_LAST_DATA_OBJECT();

// ---UPDATE--- //
// PAS D'UPDATE ICI //

// ---DELETE--- //
// PAS DE DELETE ICI //
