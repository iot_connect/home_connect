# Home Connect WEBSITE : README

### INSTALLATION
Pour installer l'environnement sur vos machines assurez vous d'avoir :
* [...]  Node js doit être installé https://nodejs.org/en/ *assurez vous de prendre **la version LTS** long term support qui aura tendance à être plus stable*
Pour vérifier l'installation de node js faites dans un terminal la commande **node --version**
* [...] Npm doit être installé https://docs.npmjs.com/downloading-and-installing-node-js-and-npm
* [...] Mongodb doit être installé https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/ assurez vous que tout fonctionne en lançant dans un terminal la commande **mongo** ou **mongod** avant de continuer
* [...] Créer votre BDD depuis le terminal mongodb en exécutant la commande use home_connect
* [...] Récupérez l'url du projet dans git https://gitlab.com/FLudovic/home_connect.git puis effectuez un git clone du projet dans un emplacement définitif que vous ne troucherez plus.
    * git clone https://gitlab.com/FLudovic/home_connect.git
* [...] Ouvrez le projet dans Visual Studio placez vous grâce au terminal ou est situé le fichier app.js qui sera le fichier hébergeant le serveur.  
Effectuez un **npm install** pour télécharger l'ensemble des dépendances.
* [...] Lancez le projet en faisant **node app.js** pour le lancer en mode de fonctionnement basique ou **npm start** pour utiliser l'extension nodemon.
* [...] Connectez-vous à **localhost:5000** sur votre navigateur pour vous connecter au site

### CONFIGURATION
Lors de l'initialisation du projet des modifications ont été apportées au projet.

* Dans package.json : Ajout dans les scripts de la fonctionnalité persist qui permet l'utilisation de nodemon.
A titre d'information les versions affichées ^1.19.0 permettent l'évolution automatique vers la version 1.20.0 lors de sa sortie.
Les versions ~1.19.0 permettent l'évolution automatique vers la version 1.19.1 lors de sa sortie mais pas de son passage à la 1.20.0 car jugé sensible.
* Dans visual studio dans file/preferences/settings recherche du parametre javascript.validate.enable pour décocher sa case (permet l'utilisation de jshint)
* Dans l'arborescence du projet création d'un fichier de configuration .gitignore https://www.toptal.com/developers/gitignore (à faire évoluer au fil du temps)
* Dans l'arborescence du projet création d'un fichier de configuration .jshintrc qui permet l'utilisation de la version es6 et désactivant les retours d'erreur sur les const.
* Installation des modules complémentaires Visual Studio EJS et Jshint (en tant qu'administrateur)


### DEPENDANCIES
Toutes les dépendances du projet peuvent être retrouvées dans le fichier ```package.json``` : 
Dépendance | Description | Installation
------------ | ------------- | -------------
```body-parser``` | Body parser est un middleware (intergiciel) qui permet de parser les requêtes reçues *req.body.foo.toString()* | `npm install --save body-parser`
```ejs``` | Ejs est un moteur de template qui nous permet de découper nos pages html en pages ou en composants réutilisables à l'aide de balises simples. | `npm install --save ejs`
```express``` | Express est une librairie permet la création et l'utilisation d'un serveur web facilement et rapidement | `npm install --save express`
```express-ejs-layouts``` | Express layouts est un complément au module ejs permettant l'utilisation d'un fichier layouts dans lequel seront appelés les métadonnées, les librairies, les scripts... | `npm install --save express-ejs-layouts`
```express-session``` | Express session est une librairie permettant la gestion des sessions et des utilisateurs (gestion des cookies, des entêtes, des sid...) | `npm install --save express-session`
```jshint``` | Js hint est un linter javascript qui permet la mise en évidence d'erreurs de syntaxe | `npm install --save jshint`
```nodemon``` | Nodemon est une librairie qui permet le relancement automatique du serveur après que l'on ait effectué des modifications puis sauvegardé. | `npm install --save nodemon`
```bcryptjs``` | Bcrypt est une librairie qui permet de chiffrer le mot de passe de l'utilisateur avant son envoie en BDD | `npm install --save bcryptjs`
```connect-flash``` | Connect flash est un middleware qui permet l'envoi de messages flash pour informer l'utilisateur (utilisé pour les formulaires de connexion) | `npm install --save connect-flash`
```mongoose``` | Mongoose est un outil complémentaire à mongodb qui permet de connecter la base | `npm install --save mongoose`
```passport``` | Passport est utilisé pour la gestion de l'authentification des utilisateurs et de leur redirection vers les chemins appropriés (est souvent utilisé en complément d'express) | `npm install --save passport`
```passport-local``` | Passport local permet de s'authentifier sur la base d'un username et password | `npm install --save passport-local`