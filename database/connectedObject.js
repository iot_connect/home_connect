// Load Models
const Object = require('../models/objectModel');

// ---ADD--- //
// Permet à un user d'ajouter un objet
const ADD_CONNECTED_OBJECT = async (itemName, topicName, imageUrl) => { // itemNam = nom de l'objet connecté | topicName = nom du topic sur lequel l'objet communique | imageUrl = lien vers l'image représentant l'objet
    const objects = await Object.create({"item" : itemName, "topic" : topicName, "image" : imageUrl});
    return objects;
};
module.exports = ADD_CONNECTED_OBJECT();

// ---GET--- //
// Retourne tous les objets connectés
const FIND_OBJECT = async () => {
    const objects = await Object.find({});
    //console.log(objects)
    return objects;
};
module.exports = FIND_OBJECT();

// Retourne un objet connecté en fournissant l'ID
const FIND_THIS_OBJECT = async id => { // Id de l'objet que l'on souhaite retourner
    const object = await Object.findOne({"_id" : id});
    //console.log(objects)
    return object;
};
module.exports = FIND_THIS_OBJECT();

// ---UPDATE--- //
// Retourne un objet connecté en fournissant l'ID
const UPDATE_THIS_OBJECT = async (id, itemName, topicName, imageUrl) => { // l'id de l'objet que l'on souhaite update | itemNam = nom de l'objet connecté | topicName = nom du topic sur lequel l'objet communique | imageUrl = lien vers l'image représentant l'objet
    const object = await Object.findOneAndUpdate({_id: id}, [{$set : {"item": itemName, "topic": topicName, "image": imageUrl}}]);
    //console.log(objects)
    return object;
};
module.exports = UPDATE_THIS_OBJECT();

// ---DELETE--- //
// Retourne un objet connecté en fournissant l'ID
const DELETE_THIS_OBJECT = async id => { // id de l'objet que l'on veut supprimer
    const object = await Object.findOneAndDelete({"_id" : id});
    //console.log(objects)
    return object;
};
module.exports = DELETE_THIS_OBJECT();
