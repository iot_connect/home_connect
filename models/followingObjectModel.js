const mongoose = require('mongoose');

const followingObjectSchema = new mongoose.Schema({
    usr: {
        type: String,
        required: true
    },
    obj: {
        type: String,
        required: true
    }
});

const followingObject = mongoose.model('following_objects', followingObjectSchema);

module.exports = followingObject;
