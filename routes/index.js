const express = require('express');
const router = express.Router();
const { ensureAuthenticated, forwardAuthenticated } = require('../config/authentication');

const dataObject = require('../models/dataObjectModel');

// Welcome Page
router.get('/', forwardAuthenticated, (req, res) => res.render('welcome'));

// Dashboard
router.get('/dashboard', ensureAuthenticated, (req, res) =>
  res.render('dashboard', {
    user: req.user,
      temperature: dataObject.find().sort({"datetime" : -1}).limit(2)
  })
);

module.exports = router;
