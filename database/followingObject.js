// Load Models
const followingObject = require('../models/followingObjectModel');

// ---ADD--- //
// Permet à un user de suivre un objet
const ADD_FOLLOWING_OBJECT = async (userID, objectID) => { // user = l'id de l'utilisateur | object = id de l'objet
    const addFollow = await followingObject.create({"usr" : userID, "obj" : objectID});
    return addFollow;
};
module.exports = ADD_FOLLOWING_OBJECT();

// ---GET--- //
// Retourne tous les abonnements d'un utilisateur
const FIND_FOLLOWING_OBJECTS_BY_USER = async (id) => {
    const findFollow = await followingObject.find({"usr" : id});
    return findFollow;
};
module.exports = FIND_FOLLOWING_OBJECTS_BY_USER();

// ---DELETE--- //
// Supprime un abonnemnt en fournissant l'ID
const DELETE_FOLLOWING_OBJECT = async id => {
    const deleteFollow = await followingObject.findOneAndDelete({"_id" : id});
    return deleteFollow;
};
module.exports = DELETE_FOLLOWING_OBJECT();
