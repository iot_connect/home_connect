const mongoose = require('mongoose');

const ObjectSchema = new mongoose.Schema({
    item: {
        type: String,
        required: true
    },
    topic: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    }
});

const Object = mongoose.model('connected_objects', ObjectSchema);

module.exports = Object;
