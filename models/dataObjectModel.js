
const mongoose = require('mongoose');

const dataObjectSchema = new mongoose.Schema({
    obj: {
        type: String,
        required: true
    },
    value: {
        type: String,
        required: true
    },
    unite: {
        type: String,
        required: true
    },
    datetime: {
        type: Date,
        required: true
    }
});

const dataObject = mongoose.model('data_objects', dataObjectSchema);

module.exports = dataObject;
