// Load Models
const User = require('../models/userModel');

// ---ADD--- //
// Permet à d'ajouter un utilisateur
const ADD_USER = async (name, email, password) => { // name = nom de l'utilisateur | email = email de l'utilisateur | password = password de l'utilisateur
    const addUser = await User.create({"name" : name, "email" : email, "password" : password});
    return addUser;
};
module.exports = ADD_USER();

// ---GET--- //
// Retourne tous les utilisateurs
const FIND_USER = async () => {
    const findUser = await User.find({});
    //console.log(objects)
    return findUser;
};

module.exports = FIND_USER();

// Retourne l'id d'un utilisateur en fournissant l'email
const FIND_THIS_USER = async email => { // Email de l'utilisateur que l'on souhaite retourner
    const findThisUser = await User.findOne({"email" : email});
    //console.log(objects)
    return findThisUser;
};
module.exports = FIND_THIS_USER();

// ---UPDATE--- //
// Met à jours un profil utilisateur à l'aide de son ID
const UPDATE_THIS_USER = async (id, name, email, password) => { // l'id de l'utilisateur que l'on souhaite update | name = nom de l'utilisateur | email = email de l'utilisateur | password = password de l'utilisateur
    const updateThisUser = await User.findOneAndUpdate({"_id" : id}, [{$set : {"name": name, "email": email, "password": password}}]);
    //console.log(updateThisUser);
    return updateThisUser;
};
module.exports = UPDATE_THIS_USER();

// ---DELETE--- //
// Retourne un utlisateur en fournissant l'ID
const DELETE_THIS_USER = async id => { // id de l'utilisateur que l'on veut supprimer
    const deleteThisUser = await User.findOneAndDelete({"_id" : id});
    //console.log(objects)
    return deleteThisUser;
};
module.exports = DELETE_THIS_USER();
