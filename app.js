const express        = require('express');
const expressLayouts = require('express-ejs-layouts');
const mongoose       = require('mongoose');
const passport       = require('passport');
const flash          = require('connect-flash');
const session        = require('express-session');
const path           = require('path');

// Load Models
const User = require('../home_connect/models/userModel');
const Object = require('../home_connect/models/objectModel');
const followingObject = require('../home_connect/models/followingObjectModel');
const dataObject = require('../home_connect/models/dataObjectModel');

const app  = express();
const PORT = process.env.PORT || 5000;

// Session parameters
const TWO_HOURS = 1000 * 60 * 60 * 2;
const SESSION_NAME     = 'sid';
const SESSION_LIFETIME = TWO_HOURS;
const SESSION_SECRET   = 'AoapPKAczA?56cz_zdh,ZIHSDC56@5s4fH-I__UDcnezoi#sngADc-dz';

// Import configuration files
require('./config/passport')(passport);
const db = require('./config/database').mongoURI;

// Connect to MongoDB
mongoose
  .connect(
    db,
    { useNewUrlParser: true ,useUnifiedTopology: true}
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));

// EJS
app.use(expressLayouts);
app.set('view engine', 'ejs');

// Express body parser
app.use(express.urlencoded({ extended: true }));

// Express session
app.use(session({
  name: SESSION_NAME,
  cookie : { 
      maxAge: SESSION_LIFETIME,
      sameSite: true,     
      secure : false
  },
  resave: true,
  saveUninitialized: true,
  secret: SESSION_SECRET
}));

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// Connect flash
app.use(flash());

// Global variables
app.use(function(req, res, next) {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg   = req.flash('error_msg');
  res.locals.error       = req.flash('error');
  next();
});

// Routes
app.use('/', require('./routes/index.js'));
app.use('/', require('./routes/users.js'));

// Set public folder for the client
app.use('/assets', express.static(path.join(__dirname, 'public')));

app.listen(PORT, console.log(`Server started on port ${PORT}`));

// DEBUT Query //

// Retourne tous les objets connectés
const findObjects = async () => {
    const objects = await Object.find({});
    return objects;
};
const pendingObjects = findObjects();
pendingObjects.then(function (returningObjects) {
    console.log("\n\nListe des objets connectés :");
    console.log(returningObjects);
});

// Retourne les data d'un objet connecté
const findDataObject = async id => {
    const dataThisObject = await dataObject.find({"obj" : id});
    return dataThisObject;
};
const pendingDataObject = findDataObject("humidityTopic");
pendingDataObject.then(function (returningDataObject) {
    console.log("\n\nListe des données d'un objet connecté :");
    console.log(returningDataObject);
});

// Récupérer l'id utilisateur
const findUsr = async email => { // Email => doit contenir l'email de l'utilisateur connecté
    const usrId = await User.find({"email": email});
    return usrId[0]['_id'];
};

// Cherche un utilisateur par son mail (s'assurer qu'il existe)
// const pendingUserId = findUsr("email.test@user.com");
// pendingUserId.then(function (returningUserId) {
//     console.log("\n\nID de l'utilisateur connecté :");
//     console.log(returningUserId);
// });

// Récupérer tous les objets suivi
const findFollowedObject = async id => { // id doit contenir l'id de l'utilisateur connecté
    const followedObject = await followingObject.find({"usr" : id});
    return followedObject;
};
// const pendingFollowingObjects = findFollowedObject("id utilisateur");
// pendingFollowingObjects.then(function (returningFollowingObject) {
//     console.log("\n\nListe des objets suivi par l'utilisateur connecté :");
//     console.log(returningFollowingObject);
// });

// Permet à un user de follow un obj
const createFollow = async (userId, objectId) => {
    const addFollow = await followingObject.create({"usr" : userId, "obj" : objectId});
    return addFollow;
};
//createFollow("...", "...") // usr = id de l'utilisateur | obj = id de l'objet connecté

// Permet à un user de follow un obj
const addDataObject = async (topic, value, unite, datetime) => {
    const addData = await dataObject.create({"obj" : topic, "value" : value, "unite" : unite, "datetime" : datetime});
    return addData;
};
//addDataObject("...", "...") // obj = topic de l'objet connecté | value = valeur retourné par l'objet connecté | unite = unité de mésure | datetime = timestamp

// Récupère les 5 dernières données remontés
const getFiveLastElements = async () => {
    const fiveLastElements = await dataObject.find().sort({"datetime" : -1}).limit(5);
    return fiveLastElements;
}
const pendingFiveLastElements = getFiveLastElements();
pendingFiveLastElements.then(function (returningFiveLastElements) {
     console.log("\n\nListe des 5 dernières données insérée");
     console.log(returningFiveLastElements);
});

// FIN QUERY //

// ----------------------------------------------------- MQTT----------------------------------------------------------------------
var mqtt = require('mqtt');
// var Topic = '#'; //subscribe to all topics
var TopicTest = 'topictest'; //subscribe to test topic
var TopicSolHumidity = 'YNOV/SOILMOISTURE/bc:dd:c2:10:7b:14/HUM'; //subscribe to humidity topic
var TopicHumidity = 'YNOV/DHT11/bc:dd:c2:10:7b:14/HUM'; //subscribe to temperature topic
var TopicTemperature = 'YNOV/DHT11/bc:dd:c2:10:7b:14/TEMP'; //subscribe to temperature topic
var TopicPhotosensor = 'YNOV/SENSORLIGHT/bc:dd:c2:10:7b:14/'; //subscribe to photosensor topic
var Broker_URL = 'mqtt://test.mosquitto.org'; // broker.mqtt-dashboard.com
var options = {
   clientId: 'email.test@user.com', // Remplacer le Client ID par l'email utilisateur
   port: 1883,
   keepalive : 60
};
var client  = mqtt.connect(Broker_URL, options);
client.on('connect', mqtt_connect);
client.on('reconnect', mqtt_reconnect);
client.on('error', mqtt_error);
client.on('message', mqtt_messsageReceived);
client.on('close', mqtt_close);
function mqtt_connect() {
    console.log("Connecting MQTT");
    client.subscribe(TopicTest, TopicHumidity, TopicTemperature, TopicSolHumidity, TopicPhotosensor, mqtt_subscribe);
}
function mqtt_subscribe(err, granted) {
    console.log("Subscribed to " + TopicTest + ", " + TopicHumidity + ", " + TopicTemperature + ", " + TopicSolHumidity + ", " + TopicPhotosensor);
    if (err) {console.log(err);}
}
function mqtt_reconnect(err) {
    console.log("Reconnect MQTT");
    if (err) {console.log(err);}
   client  = mqtt.connect(Broker_URL, options);
}
function mqtt_error(err) {
    console.log("Error!");
   if (err) {console.log(err);}
}

// Send message on the topics
const message="Ceci est un message de test";
const topic="topictest";
const timer_id=setInterval(function(){publish(topic,message);},10000);
//Publish Function
function publish(topic,msg,options) {
    console.log(msg);
    if (client.connected == true) {
        client.publish(topic, msg, options);
    }
}
// Récupère le message envoyé sur le topic "topictest" et stocke la valeur dans la BDD
async function mqtt_messsageReceived(topic, msg, packet) {
   console.log('\n\nTopic [' +  topic + '] : {"' + msg + '"}');
   //addDataObject(topic, msg, "%", Date.now()); // <= appel de la fonction d'ajout dans la BDD
   getFiveLastElements();
}
function mqtt_close() {
   console.log("Close MQTT");
}
